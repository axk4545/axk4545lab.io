# README
This is my GitLab Pages site. It is my blog and landing page. The theme and site are based on the repo and tutorial at [Barry Clark's Jekyll Now respository](https://github.com/barryclark/jekyll-now) with modifications to the header to support my custom links.

