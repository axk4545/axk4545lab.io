---
layout: page
title: About
permalink: /about/
---
I am a graduate of the Networking and Systems Administration program at Rochester Institute of Technology. I have a very strong and passionate interest in Linux and the free and open source software community. I have been using varying distributions of Linux for roughly 7 years. I am currently using Red Hat based Fedora Linux as a primary OS.

