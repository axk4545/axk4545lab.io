---
title: My Experiment with Linux on a Chromebook
layout: post
---
A couple of years ago, I was gifted an Acer C720 Chromebook. This model has the interesting feature of a user replaceable SSD. Given the size and weight difference between a chromebook and a tranditional laptop, I was eager to switch to the Chromebook. The original 16GB SSD with ChromeOS is rather limiting for the type of work I needed to do with it not to mention the fact that ChromeOS has large proprietary components. 

<!-- more -->
In an effort to remedy these percieved limits, I upgraded the SSD to 128GB and began researching how to install Linux. For Linux on a Chromebook you have a couple options. The first is to run it using something called [crouton](https://github.com/dnschneid/crouton). Crouton is an optimized Xubuntu `chroot` environment that you can download and bootstrap to run inside of ChromeOS. This approach is simple and easy to setup but has some downsides. The most notable one is the constant switching between ChromeOS and Crouton. There are all the smaller annoyances of switching such as sharing files and just the general lack of integration between the two systems. 

Your second option is to install Linux natively on the Chromebook. Archlinux has an [extensive guide](https://wiki.archlinux.org/index.php/Chrome_OS_devices) on how to do this. I elected not to install Archlinux as I was not in the mood to configure my system from scratch. From the other research I had done and the existence of Crouton, it seemed Ubuntu would be a reasonable option. I was originally going to use stock Xubuntu but then I discovered [GalliumOS](https://galliumos.org). GalliumOS is a build of Xubuntu optimized for ChromeOS hardware. Some of the kernel modules that are not used have been stripped out as well as optimizing the OS for running from an SSD. The developers have also added the ChromeOS touchpad drivers and enabled zram swap(a way of storing swap compressed in RAM). 

After taking a hiatus from using my Chromebook, I have once again returned to it and installed GalliumOS. I am planning on using it for as long as I can stand to be in the different environment from my normal setup. I may even adapt it to fit my normal setup. Whatever the case, it will have been a fun experiment in "FOSSing" a Chromebook and worst case my Chromebook will resume its role as a netflix player.
