---
title: Visiting Interlock Rochester
layout: post
---

About 2 weeks ago, a local makerspace/hackerspace here in Rochester called Interlock held a Linux workshop open to the public. Through my time volunteering at the Rochester Mini Makerfaire, I had a general familiarity with Interlock. When I heard about the workshop, I decided to attend to get a better idea of Interlock as an organization and gain exposure to the local tech community.
<!--Read More-->

I arrived at the workshop and after signing the guestbook was given a semi-formal tour of the space. The Interlock space is on the second floor of the Hungerford Building, suite 200. The building itself was originally part of a factory complex that produced flavoring syrup for soda. Due to this former life, it has a cool industrial feel that fits nicely with the makerspace aspect of its current life. Within suite 200 there are about 11 spaces. They are listed below with details where appropriate.

- Conference room: main meeting are. has a projector, whiteboard and conference table with a power strip in the center.
- Makerspace: large open area with power tools. Among these are a number of drill presses, a laser cutter and other things which I cannot recall at this time. 
- Electronics workbench: workbench area with a supply of small electronic components and tools for working with and repairing electronics. 
- Lounge: area with comfy chairs, a couch and a coffee table. used for general discussion and hanging out.
- Radio room: the space that houses the HAM radio equipment and serves as the base for K2HAX. 
- Office space: members may rent office space that allows them a room with a locked door that they can "leave as messy as they want"(according to my tour guide, Carl Schmittmann)
- Server room: the infrastructure heart of Interlock. houses all the networking and servers for the space. 

I would also like to mention two other features of the space. The first is the lightning detector. It is essentially a large semi-spherical antenna to detect variations in atmospheric static. The antenna connects to a small single-board computer in the server room that relays the data to the National Weather Service or the National Oceanic and Atmospheric Administration though I cannot remember which at this time. 

The second feature is the small library in the conference room. This library is stocked with books donated by members and checkout is based on the honor system. During my visit, I borrowed a copy of the GNU Emacs manual, sixth edition for version 19 originally published in March of 1987. I plan to return the book tomorrow during the general meeting. 

Over all, I had a good experience at Interlock and enjoyed spending time at the workshop. I stayed from around 1:00 PM until 4:00 PM.


