---
title: "Project in FOSS Development Interaction Analysis: Sprint 1"
layout: post
---

In addition to the [HFOSS](https://github.com/ritjoe/hfoss/wiki/syllabus) course I am taking that is the primary catalyst for this blog, I am also taking [Project in FOSS Development](http://bit.ly/rit-foss-projects-syllabus-2018). For the second course which I will refer to as FOSS Project, I am working on development of [TigerOS](https://web.archive.org/web/20180217161526/https://github.com/RITlug/TigerOS). 
<!--Read More-->


With TigerOS being a Fedora Remix, the developers including myself work very closely with the upstream Fedora community to ensure we are following standard procedures and practices and maintaining compatibility with them. The following is an excerpt of a discussion I had with a member of the Fedora community regarding some of their tooling. `clime` is the primary developer for a tool called [COPR](https://web.archive.org/web/20180106040140/https://copr.fedorainfracloud.org/) which allows users to create their own RPM repositories hosted on Fedora servers. The Fedora Wiki describes it as follows:

> Copr (Cool Other Package Repo) is a Fedora project to help make building and managing third party package repositories easy. The instance to be installed within Fedora Infrastructure provides Fedora maintainers with the ability to create repos of packages to build against and share with others. 


```

--BEGIN IRC EXCERPT--
 09:11 clime it won't have tests and it will maybe not be complete implementation but it shouldn't be very difficult :)
 09:11 clime not very difficult to do that basics
 09:11 axk4545 that is fine. as long as I can talk to either a builder docker container or a builder provisioned with ansible I am good.
 09:12 clime you will probably need to ssh to the docker instance and invoke copr-rpmbuild as a remote command.
 09:13 axk4545 that is fine for now.
 09:13 clime copr-rpmbuild scm --clone-url https://github.com/examplerepo --committish master 
 09:13 clime okay :)
 09:13 clime this is fairly easy so I can make it higher prio.
 09:14 axk4545 eventually I hope to have it triggered on push through jenkins or something so the box running building will also have jenkins or similar
 09:15 clime ye okay
 10:55 clime Hello, I made this branch on pagure: https://pagure.io/copr/copr/branch/copr_rpmbuild_cmdline
 10:55 clime and this copr project: https://copr.fedorainfracloud.org/coprs/clime/copr-rpmbuild-cmdline/
 10:55 axk4545 ok. I will test and see what it does. thank you.
 10:55 clime I will make some builds in https://copr.fedorainfracloud.org/coprs/clime/copr-rpmbuild-cmdline/ in a few hours. 
 10:56 clime Basically you can expect that you can invoke copr-rpmbuild scm --clone-url https://github.com/examplerepo --committish master 
 10:56 axk4545 sure. and I can also do local builds for testing using the pagure code.
 10:56 clime and it will build srpm and rpm and put the results into /var/lib/copr-rpmbuild/results/ 
 10:57 axk4545 to save you making the COPR builds of it I mean...
 10:57 axk4545 thank you so much.
 10:57 clime np
 10:57 === ========== End of backlog (20 lines) ==========
 15:39 <clime> Hello, I have pushed and build the current state: https://copr.fedorainfracloud.org/coprs/clime/copr-rpmbuild-cmdline/build/715522/
 15:39 <clime> you should be able to do: dnf copr enable clime/copr-rpmbuild-cmdline 
 15:39 <clime> install copr-rpmbuild and experiment with it
 15:40 <axk4545> thank you.

```

This discussion demonstrates two things to me. The first one is about the FOSS community as a whole. In the FOSS community is as very different than many models of software development. If you would like to see a specific feature added, you reach out to the maintainer and request it often assisting with the development of the feature where your knowledge permits. Maintainers while having varying degrees of accessibility and availability, are generally more accessible to the users than in a proprietary development model. 


The second observation is about the Fedora community specifically. One of the four common values in the Fedora community (Fedora calls them the "[Four Foundations](https://web.archive.org/web/20180214235236/https://docs.fedoraproject.org/fedora-project/project/fedora-overview.html)") is Friends. This means that Fedora acknowledges diversity and accepts new contributors and community members with open arms. `clime` showed a willingness to work with me that exemplified this value very well. 


My interactions with the Fedora community have been a largely positive experience and hope for that to continue. I look forward to working with the Fedora community and possibly increasing my involvement in the future.


Note to `dzho`: This blog post is both `blog05` for HFOSS as well as being an interaction analysis for FOSS Project.
