---
title: The Problem With Android
layout: post
---

During this week and in the past, I have had random thoughts about FOSS and some of the issues with the FOSS world and community especially in the context of the greater world. One notion I have held for a very long time is the issue of Android fragmentation and support. I love Android as a platform, even more so after my brief foray into the world of Windows Phone, but there is still something it lacks.
<!--more-->

That thing is unity. There is a degree of fragmentation in the Linux community as a whole but it is slightly different on Android. In the Android version of Linux, users are at the mercy of the OEMs and carriers. The OEMs are constantly churning out new devices and this leaves older devices lagging when it comes to system updates and most critically, security patches. Then there are the carriers. Unlike the iPhone, carriers are free to modify and add to Android as they see fit. This creates a vastly different experience for each user depending on their device and carrier. 

In order for Android update to reach a phone it must go through a long process. The rough steps are included below.

1. Update and source code released by Google
2. OEMs obtain update and add their modifications
3. OEMs build and test the update against select devices in their product line
4. OEMs distribute the update to the carriers
5. Carriers add modifications
6. Carriers build and test the update against the devices they carry and support
7. Carriers roll out the update via OTA updates

This process can take anywhere from days to months depending on the OEM and the carrier. In some cases, there may never be an update for a device. This would be ok except that many OEMs, sometimes prompted by carriers, lock the bootloader on the device before it leaves the factory. While this may increase security, it also disallows use of custom software forcing the user to replace their device if they want the latest and greatest Android.

This may be set to change with additions to Android such as [Project Treble](https://source.android.com/devices/architecture/treble) but that is not retroactive. There are still thousands of Android devices that will never be updated. 
When I first got an Android phone in I was under the impression that Android was THE platform for developers and modders. You could do whatever you wanted and change the entire system, all because of the Linux base. While it has improved since then and advances in smartphone freedom have been made, there is still a long way to go and it starts with advocating FOSS first.
