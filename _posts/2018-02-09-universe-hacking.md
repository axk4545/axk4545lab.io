---
title: Reflections on FOSS ideas in the physical world
layout: post
---

This past Wednesday I attended a talk given by RIT professor Mel Chua on the subject of "universe hacking" or changing the world to be more changeable. Mel Chua started her career as an electrical engineer working with the One Laptop Per Child project([OLPC](http://one.laptop.org/)). She has since worked with the [Fedora project](https://docs.fedoraproject.org/fedora-project/project/fedora-overview.html) and SugarLabs at RedHat and participated in the Professors of Open Source Software Experience program([POSSE](http://teachingopensource.org/)).  
<!--Read more-->

The inspiration for the POSSE program and Dr. Chua's work within it comes from a desire to bring the FOSS culture to academia. There is a disconnect between the free spirited, do it for the fun of it because we can attitude of the FOSS movement and community and the pedagogic attitude of academia. The academic world is interested in helping students learn and progress toward defined objectives and goals. The FOSS world is much more freeform.

In the talk, Dr. Chua proposed three approaches or "patches" to adapting and integrating FOSS and FOSS principals with the rest of the world.
These approaches were woven into a scenario where we are interstellar explorers and have just discovered a new planet with intelligent life.
## Patches
1. Breath Holding- Deal with it as it is.
2. Spacesuit/avatar- Use an alternative tool with the same functions that is compatible with the other ways.
3. Biodomes- Carve out a unique space where you can interact with the world in a way you want in a like-minded group.
4. Terraforming- Take over and do everything your way.
The trick is stiking a balance between the breath holding and the terraforming and that is a challenge I will be facing very soon as I go from my comfortable world using Linux and surrounded by FOSS into the world of corporate America. 

I hope that I will be able to find an approach that works for me and my employer to retain FOSS in my life as I make this transition. Maybe that means working with my employer to design a spacesuit of approved FOSS software running on my work compute,  maybe that means finding a biodome in the IT department that allows employees to run and contribute to FOSS or maybe that means finding a terraformed environment such as RedHat where FOSS is the default. However it turns out, I will strive to retain FOSS as a part of my life and identity and endeavor to spread the ideas and principles of FOSS through my work.
