---
title: Commarch Report Commentary
layout: post
---

For HFOSS the last few weeks, we have been working on a community architecture project analyzing the architcture of the people and operations side of a particular FOSS project. I chose to work on analyzing Jekyll. The first step in the process was to propose a team and project to analyze. The proposal for the team I was on can be found [here](https://web.archive.org/web/20180326222123/https://github.com/axk4545/hfoss-commarch/blob/master/teamproposal.md).
<!-- more -->

Following the proposal, we were to analyze the project using the tool [gitbyabus](https://github.com/tomheon/git_by_a_bus/tree/v2), calculate the ["Callaway Coefficient of Fail"](https://web.archive.org/web/20180104174858/https://spot.livejournal.com/308370.html) and answer some other questions about the project.

We then presented the findings to the class. The repository containing all of the artifacts from our commarch project can be found at [https://github.com/axk4545/hfoss-commarch](https://web.archive.org/web/20180326221724/https://github.com/axk4545/hfoss-commarch). The report itself is in the [report.md](https://web.archive.org/web/20180326221851/https://github.com/axk4545/hfoss-commarch/blob/master/report.md) file and the class slides are in the presentation directory of the repo and hosted at [https://gen1e.github.io/hfoss/2018/03/27/commarch.html](https://gen1e.github.io/hfoss/2018/03/27/commarch.html).
