---
title: Securely Erasing a TrueNAS X-10 Storage Appliance with GRML and nwipe
layout: post
---
In 2018 shortly after starting my post college career I had the task of securely erasing a TrueNAS X-10 storage appliance. Normally, erasing a storage appliance wouldn't be difficult. In this case though it was more of a challenge. The X-10 is a headless appliance and we didn't have any spare servers with RAID controllers compatible with the drives. I am finally posting the process I followed in the hope that others will find it as useful as I did.

<!-- more -->

**Required Tools**

- Black TrueNAS X-10 3.5mm serial to USB2.0 cable
- Linux system with serial console emulator installed
- GRML 2017.05 x64 full live USB from [http://archive.grml.org/grml64-full_2017.05.iso](http://archive.grml.org/grml64-full_2017.05.iso)

![TrueNAS X-10 storage appliance, rear view](/images/truenas.png)

**Step-by-step guide**

1. Power off the TrueNAS X-10 appliance.

2. Connect the 3.5mm end of the serial cable to port 10 in the image above. Connect the USB end to your console machine.

3. Connect the GRML live USB to the open USB port(labeled 4 in the image above) on the back of the NAS.

4. Remove the recovery drive from the USB port labeled 3 in the image above.

5. Set the serial console connection settings to the following and open the connection.

	- Serial line: `/dev/ttyUSB0`
	- Speed: 115200
	- Data bits: 8
	- Stop bits: 1
	- Parity: None
    - Flow Control: None


6. Power on the TrueNAS X-10 appliance.

7. Type  `$%^0`  at the `ESM A=\>` prompt to change the console from SCSI Enclosure Services mode to standard x86 console mode.

8. Press `Enter` twice to make the change stick.

9. After a few minutes you should see the beginning of `POST`. If you do not, poweroff and power on the TrueNAS X-10 appliance while the connection is still active.

10. When the beginning of POST is visible, hit `DEL` to enter the BIOS setup.

11. Navigate to the IntelRC tab and enter the PCH config.
    
    a. Enter the USB Configuration and change USB per port disable to `Disabled`

12. Navigate to the boot tab and change the boot order to boot from the GRML live USB  drive first. Save the changes and exit.

11. Reboot the NAS with the serial cable still connected.

12. The GRML boot menu will appear. Select the `Serial` entry under `Advanced options`.

13. Edit the `linux` line adding `toram` at the end and hit `Ctrl + x` to boot.

14.  Close the serial console session and relaunch it with the settings below.

    - Serial line: `/dev/ttyUSB0`
    - Speed: 9600
    - Data bits: 8
    - Stop bits: 1
    - Parity: None
    - Flow Control: XON/XOFF


15. The boot messages of the system should scroll up the screen.

16. Once the system has finished booting you should be at a shell prompt.

17. Type `nwipe` at the prompt and hit `Enter`.

18. Proceed as you would with DBAN on any other system.
