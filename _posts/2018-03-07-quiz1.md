---
title: HFOSS Quiz1
layout: post
---
 1.1 IRC: Internet Relay Chat
 
 1.2 FOSS: Free and Open Source Software
 
 1.3 OLPC: One Laptop Per Child
 
 1.4 FSF: Free Software Foundation
 
 1.5 PR: Pull Request
 
 1.BONUS: GNU: GNU is Not UNIX

 2.0 Git
 
 2.BONUS Mercurial
<!-- more -->

 3.0 Sugar

 4.0 XOs

 5.0 GitHub

 6.0 GitLab

 7.0 BitBucket

 8.0 d) fork

 9.0 a) repository

 10.0 c) remote
 
 11.0 e) clone
 
 12.0 b) branch
 
 13.0 a) valid
      b) invalid
      c) valid
      d) invalid
      e) invalid
      f) invalid
      g) invalid
      h) invalid
 
 14.0 Four Rs: Read, Run, Redistribute, Revise
