---
title: My Foray into Vintage Computing
layout: post
---

As one of my Christmas gifts this year, I asked my parents for a vintage Commodore 64 computer. I have always been interested in vintage or retro computing. I attribute this to the fact that I only discovered computers in the mid to late 2000s and so I missed out on the golden age of hobbyist computing where you bought kits and assembled them under the fluorescent lights of your garage or basement. 
<!--Read more-->


My first experience with a computer was running the relatively recent Windows XP. I remember playing with batch files that spawned infinite calculators and killing explorer.exe to attempt to get a DOS like feel. My enthusiasm only increased when I moved to Linux. Linux by nature has a bit of a retro vibe. I have also experimented with running AT&T UNIX System V in a virtual machine on my modern computer. That was awesome when I finally got it working but it was also my first experience with hardware compatibility issues due to age. The computers of today are vastly different from the 8086, 80286 and 80386 of the past. They have exponentially more RAM and more disk. They are still computers but a very different breed.

This brings me back to the issue of hardware standards and my Commodore 64. When I was considering and planning for what I would do with it if I should get it(I did), I did as much research as I could without the physical machine on what adapters and connectors and accessories I would need to use to unleash its full potential. In the process I discovered that computing is much easier today than it was when the Commodore was on the market. This is also a side effect of the machine being 40 or so years old. If you look at the connectors on a Commodore 64 you will not find USB, or VGA or Ethernet. The Commodore has two controller ports on the side for connecting a joystick or similar peripheral. On the rear panel are 5 or 6 connections. Among these are an IEEE-488 serial connection for a printer or disk drive, a cassette connector, an expansion port for cartridges, an RF TV output, an s-video output and a 24 pin RS-232 compatible user port(but it doesn't use a modern RS-232 connector...).


The first challenge was sorting out the video output options. I know vaguely of s-video but have only ever seen it used for camcorders, VCRs or similar equipment. All the computers I have used until now have had VGA or newer as a video output. At first I wasn't sure what to make of the RF video output option on the Commodore. The connector itself resembles an RCA video connector but is intended to connect to a TV using an old RF modulator like the kind that would be used with the "rabbit ears" antennas. I ordered a converter online that can go from s-video input to it and output to a VGA monitor. I soon discovered however that the s-video connector on the Commodore is larger than the standard s-video connector.


Now that I finally have most of it sorted out, I have time to think about the evolution of computing and the role that FOSS has played in it. In those days of computing which I described above with the hobbyist in their garage, there was a sense of openness. The hardware was bought in components and assembled using diagrams that came with the machine. Magazines were published that included source code for programs you would type or key in to your computer. The hobbyists would share their knowledge through conventions and local meetings. The one lacking thing was an organized effort by manufacturers to cater to those that believed in this freedom. Each manufacturer had their own proprietary connector or component all locked away behind lawyers and patents. As these technologies have aged, enterprising makers, modders and hackers have reversed the designs and pored over the specs and designs so that we now have public knowledge of the inner working of these early machines. 

We have also come a long way in sharing our technology. Virtually every computer made today has one of a number of I/O connectors of various types that while you may need an adapter is not specific to a manufactured or brand. The industry has changed, they realized that it is not practical to lock us in to using their products to remain compatible and avoid paying for new hardware and is far better to she the standards and compete on the merits of design and computing power. 


I hope that there will soon be a resurgence of the hobbyist and maker community and their attitude of sharing for the common good this time with corporate America at their side.

