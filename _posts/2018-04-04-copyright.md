---
title: Copyright, Copyleft and Licensing
layout: post
---

Copyright has a long and fraught history. When the Free Software Movement began and the web took off, suddenly licensing applied to digital works. This led to the creation of special licenses to protect the openness of software and digital works. A well known license is the GNU GPL.  
<!-- more -->

The GPL is easily applied to software that you run yourself on your systems. It is clear that you must provide the code to users. This clarity begins to break down when you are providing those services over a network. Since the users aren't directly running the software, it is unclear whether they are entitled to access the code. The solution to this is the GNU Affero GPL. It specifies that the license provision of sharing the code applies to software that is provided over a network as well as locally installed. 

On a related note to licensing is the way to that software licenses work in academia. Often research universities want to retain control of the projects created by students, faculty and staff. This leads them to develop custom license terms that amount to their software being freeware unless you are associated with the project. To me this flies in the face of the ideals of academia. The free thought and exchange of ideas. 

This bothered me greatly when I was working on packaging software for [TigerOS](http://tigeros.ritlug.com). A number of the pieces of software used in courses at RIT are of this special freeware designation making them incompatible with the official Fedora packaging standards of building from source locally before installing. 

While there are no simple answers to licensing and copyright questions, these are my brief thoughts and I hope that in the future academic institutions embrace free culture and free software licenses for works done through research that they wish to make public for the greater good.
